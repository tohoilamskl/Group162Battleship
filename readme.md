# Group162Battleship

## Group: 162
## Game: Battleship Remastered
### Members:
1. To Hoi Lam 3035667455
2. Ng Chun Hei 3035690634

### Game description:
* A strategy game where you will be guessing the coordinates of the AI's ships. If you guessed all the positions of the AI's ships first, you win. Otherwise, you lose.

* Each side has a 10x10 board and 5 classes of ships to put on it:
    * an Aircraft carrier which takes up 5 spaces,
    * a battleship which takes up 4 spaces,
    * a heavy cruiser which takes up 3 spaces,
    * a submarine which takes up 3 spaces,
    * a destroyer which takes up 2 spaces.

###  Game rules:
* Each turn you will guess the location of your opponent's ship by marking a position on a 10x10 board

### Game features:
* Randomly generate a board for both AI and player (Requirement 1)
* The game will records all the previous game history into a txt file (Requirement 4)
    * Recording your name, win/lose, number of moves used
* Then the program can retrieve the records from the txt file into a data structure, for displaying the records (Requirement 2)
* Codes to display the boards, and codes to display the game records will be in a different file than the main program codes (requirement 5)
* An AI for computer's moves
